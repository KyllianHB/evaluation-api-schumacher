## Prérequis

- installer [node.js](https://nodejs.org/en)

- installer [Docker](https://www.docker.com/get-started/) et [Compose](https://docs.docker.com/compose/)

## Initialisation du projet
( Avoir docker de démarré)

1. **Se placer dans le dossier "api" et executer la commande :** 
`npm install`

2. **Se placer à la racine du projet et executer la commande :** 
`docker compose up -d`

3. **Accèder au server de base de donnée**
	
	a. Acceder à adminer via :
    http://localhost:5003/
	
	b. Utilisateur : root,
		Mot de passe : root

	c. Choisir "mydb"

	d. Copier coller le script "init.sql" dans la section "Requête SQL" de adminer pour créer la bdd, le fichier est présent à la racine du projet

4. **Acceder à swagger** 

	a. Se placer dans le dossier "api" et executer la commande :
	`npm run swager-autogen`

	b. Accèder à la doc :
	http://localhost:5001/doc/

**=> Envoy !**

## Lancer le projet 

1. **Se placer à la racine du projet et exectuer la commande :** 
`docker compose up -d`

## PS

- Je sais pas pourquoi mais mes requêtes "Post" ne fonctionnaient pas sur swagger, j'ai utilisé postman à la place.
- J'me suis pris la tête à mettre l'utilisateur dans le retour des reservations, j'aurais peut être pas dû car ça m'a compliqué la vie haha.