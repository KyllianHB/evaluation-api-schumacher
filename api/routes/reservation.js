var express = require('express');
var router = express.Router();
var connection = require('../db');
var hal = require('../hal')

/* 
* Détail d'une reservation 
* GET /reservation/{id} 
*/
router.get('/reservation/:id', function (req, res, next) {

    connection.query('SELECT * FROM reservation WHERE id = ?;', [req.params.id], (error, rowsResa, fields) => {
        if (error) {
          console.error('Error connecting: ' + error.stack);
          return;
        }

        if(rowsResa[0] != [{}]) {

            connection.query('SELECT id, pseudo FROM user WHERE id = ?;', [rowsResa[0].user_id], (error, rowsUser, fields) => {
                const reservationObject = {
                    id: rowsResa[0].id,
                    idConcert: rowsResa[0].concert_id,
                    date: rowsResa[0].date_reservation,
                    statut: rowsResa[0].statut,
                    utilisateur: error!=null ? 'aucune utilisateur' : rowsUser[0]
                }

                const resourceObject = {
                    "_links": [],
                    "_embedded": {
                        "reservation": (hal.mapReservationtoRessourceObject(reservationObject, req.protocol + '://' + req.get('host')))
                    }
                }
                // res.status(200).set('Content-Type', 'text/html').send('GET /concerts/{id}')
                res.set('Content-Type', 'application/hal+json');
                res.status(200);
                res.json(resourceObject);
            })

        } else {
            res.set('Content-Type', 'application/hal+json');
            res.status(200);
            res.json({error: 'Aucunes réservations'});
        }
      })
})

/**
* Annuler une reservation
* PUT /reservation/:id/annuler
*/
router.put('/reservation/:id/annuler', function (req, res, next) {

    // Récupération des informations de la reservation
    connection.query('SELECT * FROM reservation WHERE id = ?;', [req.params.id], (error, rowsResa, fields) => {
        if (error) {
        console.error('Error connecting: ' + error.stack);
        return;
        }

        if(rowsResa[0] != [{}]) {
            // Récupération des informations de l'utilisateur
            connection.query('SELECT id, pseudo FROM user WHERE id = ?;', [rowsResa[0].user_id], (error, rowsUser, fields) => {

                //Update de la reservation à l'état 'annulée'
                connection.query(`UPDATE reservation SET statut = 'annulée' WHERE id = ?;`, rowsResa[0].id, (error, rows, fields) => {
                    if (error) {
                      console.error('Error connecting: ' + error.stack);
                      return;
                    }
                    
                    if(rows.affectedRows != 0) {

                        const reservationObject = {
                            id: rowsResa[0].id,
                            idConcert: rowsResa[0].concert_id,
                            date: rowsResa[0].date_reservation,
                            statut: 'annulée',
                        utilisateur: error!=null ? 'aucuns utilisateur' : rowsUser[0]
                        }
                        
                        const resourceObject = {
                            "_links": [],
                            "_embedded": {
                                "reservation": (hal.mapReservationtoRessourceObject(reservationObject, req.protocol + '://' + req.get('host'), 'annulée'))
                            }
                        }
                        
                        console.log(error)
                        console.log(rows)
                        console.log(reservationObject)
                        console.log(resourceObject)
                        
                        res.set('Content-Type', 'application/hal+json');
                        res.status(200);
                        res.json(resourceObject);
                    } else {
                        res.set('Content-Type', 'application/hal+json').res.status(500).res.json({error: 'Update failed'});
                    }
                })   
            })
            
        } else {
            res.set('Content-Type', 'application/hal+json');
            res.status(200);
            res.json({error: 'Aucunes réservations'});
        }
    })
  
})

/**
* Confirmer une reservation
* PUT /reservation/:id/confirmer
*/
router.put('/reservation/:id/confirmer', function (req, res, next) {

    // Récupération des informations de la reservation
    connection.query('SELECT * FROM reservation WHERE id = ?;', [req.params.id], (error, rowsResa, fields) => {
        if (error) {
        console.error('Error connecting: ' + error.stack);
        return;
        }

        if(rowsResa[0] != [{}]) {
            // Récupération des informations de l'utilisateur
            connection.query('SELECT id, pseudo FROM user WHERE id = ?;', [rowsResa[0].user_id], (error, rowsUser, fields) => {

                //Update de la reservation à l'état 'confirmée'
                connection.query(`UPDATE reservation SET statut = 'confirmée' WHERE id = ?;`, rowsResa[0].id, (error, rows, fields) => {
                    if (error) {
                      console.error('Error connecting: ' + error.stack);
                      return;
                    }
                    
                    //Vérification de la bonne execution de l'update
                    if(rows.affectedRows != 0) {
                    
                        const reservationObject = {
                            id: rowsResa[0].id,
                            idConcert: rowsResa[0].concert_id,
                            date: rowsResa[0].date_reservation,
                            statut: 'confirmée',
                            utilisateur: error!=null ? 'aucuns utilisateur' : rowsUser[0]
                        }
                        
                        const resourceObject = {
                            "_links": [],
                            "_embedded": {
                                "reservation": (hal.mapReservationtoRessourceObject(reservationObject, req.protocol + '://' + req.get('host'), 'confirmée'))
                            }
                        }
                        
                        console.log(error)
                        console.log(rows)
                        console.log(reservationObject)
                        console.log(resourceObject)
                        
                        res.set('Content-Type', 'application/hal+json');
                        res.status(200);
                        res.json(resourceObject);
                    } else {
                        res.set('Content-Type', 'application/hal+json').res.status(500).res.json({error: 'Update failed'});
                    }
                })   
            })
            
        } else {
            res.set('Content-Type', 'application/hal+json');
            res.status(200);
            res.json({error: 'Aucunes réservations'});
        }
    })
  
})

/**
* Liste des reservations confirmées pour un concert donné
* GET /reservation/concert/:id
*/

// Essaie avec utilisateur associé
router.get('/reservation/concert/:id', function (req, res, next) {

    //Update de la reservation à l'état 'confirmée'
    connection.query(`SELECT * FROM reservation WHERE statut = 'confirmée' AND concert_id = ?;`, [req.params.id], (error, rows, fields) => {
        if (error) {
            console.error('Error connecting: ' + error.stack);
            return;
        }
        
        //Vérification si des réservations existes
        if(rows != [{}]) {
            let listReservationObject = [];

            // fonction pour exécuter une requête de base de données et retourner une promesse
            function getUserData(userId) {
              return new Promise((resolve, reject) => {
                connection.query('SELECT id, pseudo FROM user WHERE id = ?', [userId], (error, rows, fields) => {
                  if (error) {
                    reject(error);
                  } else {
                    resolve(rows);
                  }
                });
              });
            }

            Promise.all(rows.map(row => {
              
                return getUserData(row.user_id).then(rowsUser => {

                    let reservationObject = {
                        id: row.id,
                        idConcert: row.concert_id,
                        date: row.date_reservation,
                        statut: row.statut,
                        utilisateur: error != null ? 'aucuns utilisateur' : rowsUser
                    }

                    let reservationsObject = hal.mapReservationtoRessourceObject(reservationObject, req.protocol + '://' + req.get('host'));
                    listReservationObject.push(reservationsObject)

                });

            })).then(() => {

                // construction de l'objet ressource une fois que toutes les requêtes soient passées
                const resourceObject = {
                    "_links": [],
                    "_embedded": {
                    "reservation": listReservationObject
                    }
                };

                res.set('Content-Type', 'application/hal+json');
                res.status(200);
                res.json(resourceObject);

            }).catch(error => {
              console.error(error);
              res.sendStatus(500);
            });
        } else {
            res.set('Content-Type', 'application/hal+json').res.status(404).res.json({error: 'Aucunes réservations'});
        }
    })   

 })
module.exports = router;