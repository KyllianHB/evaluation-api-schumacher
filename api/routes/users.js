var express = require('express');
var router = express.Router();
var connection = require('../db')
var hal = require ('../hal')

/* GET users listing. */
router.get('/users', function (req, res, next) {

  connection.query('SELECT * FROM User;',  (error, rows, fields) => {

    const userObject = {
      "_links": [],
      "_embedded": {
        "concerts": (rows.map(row => hal.mapUtilisateurtoResourceObject(row, req.protocol + '://' + req.get('host'))))
      }
    }

    res.set('Content-Type', 'application/hal+json');
    res.status(200);
    res.json(userObject);
  })
});

module.exports = router;
