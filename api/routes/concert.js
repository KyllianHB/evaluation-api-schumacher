var express = require('express');
var router = express.Router();
var connection = require('../db');
var hal = require('../hal')

/* GET /concerts */
router.get('/concerts', function (req, res, next) {

  // #swagger.summary = "Liste des concerts"

  connection.query('SELECT * FROM concert;', (error, rows, fields) => {

    if (error) {
      console.error('Error connecting: ' + error.stack);
      return;
    }

    //Fabriquer Ressource Object Concerts en respectant la spec HAL
    const concertsResourceObject = {
      "_links": [],
      "_embedded": {
        "concerts": (rows.map(row => hal.mapConcertoResourceObject(row, req.protocol + '://' + req.get('host'))))
      }
    }

    res.set('Content-Type', 'application/hal+json');
    res.status(200);
    res.json(concertsResourceObject);
  })
})

/* GET /concert/{id} */
router.get('/concert/:id', function (req, res, next) {

  //:id : identifiant primaire en base. Ici, on devra rajouter sur la regex 'que des caractères numériques'

  //Attention aux injections SQL !

  connection.query('SELECT * FROM concert WHERE id = ?;', [req.params.id], (error, rows, fields) => {
    if (error) {
      console.error('Error connecting: ' + error.stack);
      return;
    }
    
    console.log(rows)

    const resourceObject = {
      "_links": [],
      "_embedded": {
        "concert": (hal.mapConcertoResourceObject(rows[0], req.protocol + '://' + req.get('host')))
      }
    }
    // res.status(200).set('Content-Type', 'text/html').send('GET /concerts/{id}')
    res.set('Content-Type', 'application/hal+json');
    res.status(200);
    res.json(resourceObject);
  })
})

/**
* Créer une reservation pour le concert
* POST /concerts/:name/reservation
*/
router.post('/concerts/:id/reservation', function (req, res, next) {
  /* #swagger.parameters['pseudo'] = {
        in: 'formData',
        description: 'Le pseudo de l\'utilisateur qui effectue la réservation',
        required: 'true',
        type: 'string',
        format: 'application/x-www-form-urlencoded',
} */

  //1. On doit récupérer la représentation du client: le pseudo de l'utilisateur qui reserve
  if (!req.body.pseudo) {
    res.status(400).set('Content-Type', 'application/hal+json').send('{ "reponse": "Requête invalide, veuiller fournir le pseudo de l\'utilisateur"}')
  }

  //2. Identification : 
  connection.query('SELECT id, pseudo FROM user WHERE pseudo = ?;', [req.body.pseudo], (error, rows, fields) => {
    if (error) {
      // res.status(500).set('Content-Type', 'application/hal+json').send('{ "reponse": "Erreur lors de la requête"}')
      return;
    }

    //Si l'identification échoue
    if (rows.length === 0) {
      res.status(400).set('Content-Type', 'application/hal+json').send('{ "reponse": "Requête invalide, l\'utilisateur n\'existe pas"}')
      return
    }
    const user = rows
    const userId = parseInt(rows.map(row => row.id))
   
    //3. Vérifier qu'il n'y pas déjà de reservation pour ce concert et cet utilisateur
    connection.query('SELECT COUNT(*) as nbReservation FROM reservation r INNER JOIN user u ON r.user_id = u.id INNER JOIN concert c ON c.id = r.concert_id WHERE u.pseudo = ? AND c.id = ?;', [req.body.pseudo, req.params.id], (error, rows, fields) => {

      const nbReservations = parseInt(rows.map(row => row.nbReservation))

      if(nbReservations > 0){
        res.status(400).set('Content-Type', 'application/hal+json').send(`{ "reponse": "Requête valide: l'utilisateur ${req.body.pseudo} a déjà une reservation pour ce concert"}`)
      return
      }

      //4. Check qu'il reste des places disponibles
      connection.query(`SELECT SUM((select nb_places from concert where id = ?) - (SELECT count(*) from reservation where statut != 'annulée' AND concert_id = ?)) as nb_places_dispo`, [req.params.id, req.params.id], (error, rows, fields) => {
        const nbPlacesDispo = parseInt(rows.map(row => row.nb_places_dispo))

        if( nbPlacesDispo === 0){
          res.status(400).set('Content-Type', 'application/hal+json').send(`{ "reponse": "Requête valide: Plus de places disponibles"}`)
          return
        }

      })

      //5. Création d'une reservation
      const now = new Date();
      const sqlFormattedDateTime = now.toISOString().slice(0, 19).replace('T', ' ');

      connection.query(`INSERT INTO reservation (user_id, concert_id, statut, date_reservation) VALUE (?, ?, 'à confirmer', ?)`, [userId, req.params.id, sqlFormattedDateTime], (error, rows, fields) => {      
        //La réservation est créée avec le statut 'à confirmer' par défaut

        //6. Réponse (Resource Object)
        //Fabriquer un Ressource Object reservation en respectant la spec HAL
        const reservationData = {
          "id": rows.insertId,
          "idConcert": req.params.id,
          "date": sqlFormattedDateTime,
          "statut": 'à confirmer',
          "utilisateur": user[0]
        }
        
        const concertsResourceObject = {
          "_links": [],
          "_embedded": {
            "reservation": (hal.mapReservationtoRessourceObject(reservationData, req.protocol + '://' + req.get('host')))
          },
          "_response": "Réservation créée avec succès :)"
        }

        res.status(201).set('Content-Type', 'application/hal+json').json(concertsResourceObject);
      })
    })
  })



})

module.exports = router;
