/**
 * Export des fonctions utils hal (représentations)
 */

/**
 * Retourne un Link Object
 * @param {*} url 
 * @param {*} type 
 * @param {*} name 
 * @param {*} templated 
 * @param {*} deprecation 
 * @returns 
 */
function halLinkObject(url, type = '', name = '', templated = false, deprecation = false) {

    return {
        "href": url,
        "templated": templated,
        ...(type && { "type": type }),
        ...(name && { "name": name }),
        ...(deprecation && { "deprecation": deprecation })
    }
}

/**
 * Retourne une représentation Ressource Object (HAL) d'un concert
 * @param {*} concertData Données brutes d'un concert
 * @param {*} baseURL url de l'api
 * @returns un Ressource Object Concert (spec HAL)
 */
function mapConcertoResourceObject(concertData, baseURL) {

    /**
     * A faire: requêter le nombre de reservations pour calculer le nombre de places disponibles
     * Attention a l'async !
     */
    const reservations = 0

    const resourceObject = {
        "_links": [{
            "self": halLinkObject(baseURL + '/concerts' + '/' + concertData.id, 'string', 'Les informations d\'un concert'),
            "reservation": halLinkObject(baseURL + '/concerts' + '/' + concertData.id + '/reservation', 'string')
        }],
        "_embedded": {
            "id": concertData.id,
            "date": concertData.date_debut,
            "nb_places": concertData.nb_places,
            "nb_places_disponibles": parseInt(concertData.nb_places) - reservations,
            "lieu": concertData.lieu,
            "description": concertData.description
        }
    }

    return resourceObject
}

/**
 * Retourne un Resource Object d'un utilisateur
 * @param {*} utilisateurData Données brutes d'un utilisateur
 * @param {*} baseURL url de l'api
 * @returns un Ressource Object Utilisateur (spec HAL)
 */
function mapUtilisateurtoResourceObject(utilisateurData, baseURL) {

    return {
        "_links": [{
            "self": halLinkObject(baseURL + '/utilisateurs' + '/' + utilisateurData.pseudo, 'string'),
        }],
        "_embedded": {
            "pseudo": utilisateurData.pseudo
        }
    }

}

/**
 * Retourne un Resource Object d'une reservation
 * @param {*} reservationData
 * {
 *   id: int => id de la reservation
 *   idConcert: int => id du concert reservé
 *   date: string => date de la réservation 
 *   statut: string => Etat de la réservation
 *   utilisateur: object => objet utilisateur
 * }
 * @param {*} baseURL url de l'api
 * @returns un Ressource Object Reservation (spec HAL)
 */
function mapReservationtoRessourceObject(reservationData, baseURL, selfUrl) {
    if(reservationData != {}) {

        let _links = [{
            "self": halLinkObject(baseURL + '/reservation' + '/' + reservationData.id, 'string', 'Détail de la réservation'),
            "concert": halLinkObject(baseURL + '/concert' + '/' + reservationData.idConcert, 'string', 'Les informations du concert'),
            "confirmer": halLinkObject(baseURL + '/reservation' + '/' + reservationData.id + '/confirmer', 'string', 'Confirmer la réservation'),
            "annuler": halLinkObject(baseURL + '/reservation' + '/' + reservationData.id + '/annuler', 'string', 'Annuler la réservation'),
        }]

        switch (reservationData.statut) {
            case 'confirmée':

            _links = [{
                    "self": halLinkObject(baseURL + '/reservation' + '/' + reservationData.id + '/confirmer', 'string', 'Confirmer la réservation'),
                    "concert": halLinkObject(baseURL + '/concert' + '/' + reservationData.idConcert, 'string', 'Les informations du concert'),
                    "annuler": halLinkObject(baseURL + '/reservation' + '/' + reservationData.id + '/annuler', 'string', 'Annuler la réservation')
                }]
                
            break;
            case 'annulée':
                _links = [{
                    "self": halLinkObject(baseURL + '/reservation' + '/' + reservationData.id + '/annuler', 'string', 'Annuler la réservation'),
                    "concert": halLinkObject(baseURL + '/concert' + '/' + reservationData.idConcert, 'string', 'Les informations du concert'),
                    "confirmer": halLinkObject(baseURL + '/reservation' + '/' + reservationData.id + '/confirmer', 'string', 'Confirmer la réservation')
                }]
            break;            
        }
        // "_links": [{
        // }]
        return {
            _links,
            "_embedded": [{
                "id": reservationData.id,
                "date": reservationData.date,
                "statut": reservationData.statut,
                "utilisateur": reservationData.utilisateur
            }]
        }

    } else {
        return [{}]
    }
}

module.exports = { 
    halLinkObject, 
    mapConcertoResourceObject, 
    mapUtilisateurtoResourceObject,
    mapReservationtoRessourceObject 
};
